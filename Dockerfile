FROM phusion/baseimage:latest
LABEL org.label-schema.name="Xenial Desktop" \
      org.label-schema.description="Xenial VNC tor Desktop edition" \
      org.label-schema.vcs-url="https://gitlab.com/tigefa/ubuntu-tor-vnc" \
      org.label-schema.vendor="Sugeng Tigefa" \
      org.label-schema.schema-version="1.0" \
      maintainer="tigefa@gmail.com"

# Install vnc in order to create a 'fake' display and firefox, tor-browser packages (get deps)
# We need sudo because some post install stuff runs with tor
COPY sources.list /etc/apt/sources.list
RUN apt-get update && \
	apt-get install -y sudo wget netcat whois tree figlet curl apt-transport-https dirmngr locales locales-all xz-utils gnupg tightvncserver xterm fluxbox ca-certificates \
	libasound2 libdbus-glib-1-2 libgtk2.0-0 libgtk2.0-dev libgtk-3-dev libxrender1 libxt6

RUN apt-get install -y sudo curl wget apt-utils language-pack-gnome-en language-pack-gnome-id command-not-found command-not-found-data gnupg gnupg-agent gnupg2 apt-transport-https ca-certificates software-properties-common tzdata
RUN add-apt-repository ppa:jonathonf/backports -y && \
    add-apt-repository ppa:jonathonf/aria2 -y && \
    add-apt-repository ppa:jonathonf/python-2.7 -y && \
    add-apt-repository ppa:deadsnakes/ppa -y && \
    add-apt-repository ppa:mc3man/xerus-media -y && \
    add-apt-repository ppa:mc3man/mpv-tests -y && \
    add-apt-repository ppa:git-core/ppa -y && \
    add-apt-repository ppa:mercurial-ppa/releases -y && \
    add-apt-repository ppa:dominik-stadler/subversion-1.9 -y && \
    add-apt-repository ppa:ubuntu-toolchain-r/test -y && \
    add-apt-repository ppa:longsleep/golang-backports -y && \
    add-apt-repository ppa:brightbox/ruby-ng -y && \
    add-apt-repository ppa:apt-fast/stable -y && \
    add-apt-repository ppa:numix/ppa -y && \
    add-apt-repository ppa:numix/numix-daily -y && \
    add-apt-repository ppa:qbittorrent-team/qbittorrent-unstable -y && \
    add-apt-repository ppa:gnome-terminator/nightly-gtk3 -y && \
    add-apt-repository ppa:noobslab/macbuntu -y && \
    add-apt-repository ppa:transmissionbt/ppa -y && \
    add-apt-repository ppa:wfg/0ad -y && \
    add-apt-repository ppa:obsproject/obs-studio -y && \
    add-apt-repository ppa:peek-developers/stable -y && \
    add-apt-repository ppa:otto-kesselgulasch/gimp -y && \
    add-apt-repository ppa:uget-team/ppa -y && \
    add-apt-repository ppa:clipgrab-team/ppa -y && \
    add-apt-repository ppa:neovim-ppa/stable -y && \
    add-apt-repository ppa:certbot/certbot -y && \
    add-apt-repository ppa:webupd8team/terminix -y && \
    add-apt-repository ppa:webupd8team/java -y && \
    add-apt-repository ppa:nilarimogard/webupd8 -y

RUN sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
RUN sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger xenial main > /etc/apt/sources.list.d/passenger.list'
RUN sudo sh -c 'echo deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-5.0 main > /etc/apt/sources.list.d/llvm.list'
RUN sudo sh -c 'echo deb-src http://apt.llvm.org/xenial/ llvm-toolchain-xenial-5.0 main >> /etc/apt/sources.list.d/llvm.list'
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key| sudo apt-key add -
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
#RUN gpg --keyserver keys.gnupg.net --recv A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89
#RUN gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | sudo apt-key add -
#RUN gpg2 --recv A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89
#RUN gpg2 --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | apt-key add -
RUN echo "deb [trusted=yes] https://deb.torproject.org/torproject.org xenial main" | sudo tee /etc/apt/sources.list.d/tor-org.list
RUN echo "deb-src [trusted=yes] https://deb.torproject.org/torproject.org xenial main" | sudo tee -a /etc/apt/sources.list.d/tor-org.list
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
RUN sudo apt-get install -y tor deb.torproject.org-keyring
RUN sudo apt-get -y install aria2 nano python3-xlib python-apt
RUN sudo ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
RUN sudo dpkg-reconfigure -f noninteractive tzdata
RUN apt-get install -yqq tightvncserver gnome-system-monitor xubuntu-desktop xubuntu-default-settings
RUN apt-get install -yqq landscape-client landscape-common nodejs yarn wget curl netcat aria2 whois figlet git git-lfs p7zip p7zip-full zip unzip rar unrar
RUN apt-get install -yqq tilix
RUN ln -s /etc/profile.d/vte-2.91.sh /etc/profile.d/vte.sh && \
    update-alternatives --set x-terminal-emulator $(which tilix)
RUN apt-get install -yqq *powerline*
#RUN apt-get install -yqq *dropbox*
RUN apt-get install -yqq *numix* breeze-cursor-theme uget uget-integrator youtube-dl
RUN wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
RUN echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
RUN apt-get update
RUN apt-get install -yqq sublime-text python-pip python3-pip net-tools bash bash-completion
RUN sudo -E -H pip install powerline-shell
RUN apt-get install -yqq dconf-cli dconf-editor dconf-tools clipit xclip flashplugin-installer
RUN apt-get install -yqq xfce4-*-plugin

## Docker and Kubernetes
RUN wget -qO- https://get.docker.com/ | sh
RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
RUN sudo touch /etc/apt/sources.list.d/kubernetes.list 
RUN echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
RUN sudo apt-get update
RUN sudo apt-get install -y kubectl

COPY caffeine-plus.deb /tmp/caffeine-plus.deb
RUN dpkg -i /tmp/caffeine-plus.deb

## add apt-metalink
RUN wget https://github.com/tatsuhiro-t/apt-metalink/raw/master/apt-metalink -O /usr/local/bin/apt-metalink && \
    chmod +x /usr/local/bin/apt-metalink && \
    echo "alias apt-metalink='yes | sudo /usr/local/bin/apt-metalink install'" >> /etc/profile.d/apt-metalink.sh && \
    chmod +x /etc/profile.d/apt-metalink.sh

RUN sudo apt-get update && \
    apt-get upgrade -y && \
    apt-get dist-upgrade -y && \
    apt-get autoremove -y --purge *appstream* && \
    apt-get autoremove -y && \
    apt-get autoclean -y && \
    apt-get clean -y

ENV TOR_VERSION 8.0.2
ENV TOR_FINGERPRINT 0x4E2C6E8793298290

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /root/.cache /root/.config /root/.local /root/.gnupg

#### Get and extract Tor Browser bundle with verification####
RUN wget -q "https://dist.torproject.org/torbrowser/${TOR_VERSION}/tor-browser-linux64-${TOR_VERSION}_en-US.tar.xz" && \
	tar xf "tor-browser-linux64-${TOR_VERSION}_en-US.tar.xz" && \
	rm -rf "tor-browser-linux64-${TOR_VERSION}_en-US.tar.xz"
####/Bundle####

####user section####
ENV USER developer
ENV HOME "/home/$USER"
RUN echo 'developer ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo '%developer ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo 'sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo 'www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo '%www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN useradd --create-home --home-dir $HOME --shell /bin/bash $USER && \
	mkdir $HOME/.vnc/
RUN usermod -aG sudo $USER && \
    usermod -aG root $USER && \
    usermod -aG adm $USER && \
    usermod -aG debian-tor $USER && \
    usermod -aG www-data $USER && \
    usermod -aG docker $USER

COPY vnc.sh $HOME/.vnc/
COPY xstartup $HOME/.vnc/
RUN chmod 760 $HOME/.vnc/vnc.sh $HOME/.vnc/xstartup && \
	chown -R $USER:$USER $HOME tor-browser_en-US

USER "$USER"

####Setup a VNC password####
RUN	echo vncpassw | vncpasswd -f > ~/.vnc/passwd && \
	chmod 600 ~/.vnc/passwd

###/user section####
ENV FIREFOX_ESR_VERSION 60.2.0
ENV FIREFOX_ESR_LANG id
COPY tilix.dconf $HOME/tilix.dconf
#RUN dconf load /com/gexperts/Tilix/ < $HOME/tilix.dconf
RUN mkdir -p /home/$USER/.local/bin /home/$USER/.local/share/applications
COPY menulibre-firefox-esr.desktop /home/$USER/.local/share/applications/menulibre-firefox-esr.desktop
RUN cd /home/$USER/.local/bin && wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip; unzip *.zip; rm -rf *.zip
RUN cd $HOME && mkdir -p $HOME/.config/powerline-shell/ && wget https://gitlab.com/snippets/1750446/raw -O $HOME/.config/powerline-shell/config.json
RUN cd $HOME && wget http://ftp.mozilla.org/pub/firefox/releases/${FIREFOX_ESR_VERSION}esr/linux-x86_64/${FIREFOX_ESR_LANG}/firefox-${FIREFOX_ESR_VERSION}esr.tar.bz2 && tar -xf firefox-${FIREFOX_ESR_VERSION}esr.tar.bz2 && rm -rf firefox-${FIREFOX_ESR_VERSION}esr.tar.bz2
#RUN cd /home/$USER/.local/bin && wget https://download.docker.com/linux/static/stable/x86_64/docker-18.06.1-ce.tgz && tar --strip 1 -xf docker-18.06.1-ce.tgz && rm -rf docker-18.06.1-ce.tgz
#RUN cd /home/$USER/.local/bin && curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && chmod +x ./kubectl
RUN sudo chown -fR $USER:$USER $HOME

EXPOSE 5901
####/Setup VNC####

CMD ["/home/developer/.vnc/vnc.sh"]
